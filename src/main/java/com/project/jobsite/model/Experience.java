package com.project.jobsite.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "Experience")
public class Experience {
	
	@Id
	@GeneratedValue
	@Column(name = "experienceID", nullable = false)
	private int experienceID;
	
//	@Column(name = "resumeID", nullable = false)
//	private int resumeID;
	
	@Column(name = "title", nullable = false, length = 150)
	private String title;
	
	@Column(name = "description", nullable = false, length = 255)
	private String description;
	
	@Column(name = "since", nullable = false)
	@Temporal(TemporalType.DATE)
	private Calendar since;
		
	@Column(name = "till")
	@Temporal(TemporalType.DATE)
	private Calendar till;
	
	@Column(name = "active", nullable = false)
	private int active;

	public int getExperienceID() {
		return experienceID;
	}

	public void setExperienceID(int experienceID) {
		this.experienceID = experienceID;
	}

//	public int getResumeID() {
//		return resumeID;
//	}
//
//	public void setResumeID(int resumeID) {
//		this.resumeID = resumeID;
//	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Calendar getSince() {
		return since;
	}

	public void setSince(Calendar since) {
		this.since = since;
	}

	public Calendar getTill() {
		return till;
	}

	public void setTill(Calendar till) {
		this.till = till;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

}
