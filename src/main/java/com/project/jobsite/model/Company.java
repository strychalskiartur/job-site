package com.project.jobsite.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "Company")
public class Company {
	
	@Id
	@GeneratedValue
	@Column(name = "companyID", nullable = false)	
	private int companyID;
	
	@Column(name = "userID", nullable = false)
	private int userID;
	
	@Column(name = "nip", nullable = false)
	private int nip;
	
	@Column(name = "name", nullable = false, length = 150)
	private String name;
	
	@Column(name = "description", length = 255)
	private String description;
	
	@Column(name = "phone", length = 11)
	private String phone;
	
	@Column(name = "email", nullable = false, length = 255)
	private String email;
	
	public int getCompanyID() {
		return companyID;
	}

	public void setCompanyID(int companyID) {
		this.companyID = companyID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public int getNip() {
		return nip;
	}

	public void setNip(int nip) {
		this.nip = nip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@ManyToMany
	private List<User> users;

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	@OneToMany
	@JoinColumn(name = "offertID")
	private List<Offert> offerts;

	public List<Offert> getOfferts() {
		return offerts;
	}

	public void setOfferts(List<Offert> offerts) {
		this.offerts = offerts;
	}
	


}
