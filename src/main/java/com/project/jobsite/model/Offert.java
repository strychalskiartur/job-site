package com.project.jobsite.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Offert")
public class Offert {
	
	@Id
	@GeneratedValue
	@Column(name = "offertID", nullable = false)
	private int offertID;
	
	@Column(name = "jobTitle", nullable = false, length = 150)
	private String jobTitle;
	
//	@Column(name = "companyID", nullable = false)
//	@ManyToOne()
//	@JoinColumn(name="companyID")
//	private Company companyID;
	
	@Column(name = "contact", length = 255)
	private String contact;
	
	@Column(name = "description", length = 255)
	private String description;

	@Column(name = "benefits", length = 255)
	private String benefits;
	
	@Column(name = "requirements", length = 255)
	private String requirements;
	
	@Column(name = "intUserID")
	private  int intUserID;
	
	public int getOffertID() {
		return offertID;
	}
	public void setOffertID(int offertID) {
		this.offertID = offertID;
	}
	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
//	public Company getCompanyID() {
//		return companyID;
//	}
//	public void setCompanyID(Company companyID) {
//		this.companyID = companyID;
//	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getBenefits() {
		return benefits;
	}
	public void setBenefits(String benefits) {
		this.benefits = benefits;
	}
	public String getRequirements() {
		return requirements;
	}
	public void setRequirements(String requirements) {
		this.requirements = requirements;
	}
	public int getIntUserID() {
		return intUserID;
	}
	public void setIntUserID(int intUserID) {
		this.intUserID = intUserID;
	}
	
	@ManyToMany(mappedBy = "userID")
	private List<User> users;

	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	
	

}
