package com.project.jobsite.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "User")
public class Resume {
	
	@Id
	@GeneratedValue
	@Column(name = "resumeID", nullable = false)
	private int resumeID;
	
	@Column(name = "userID", nullable = false, length = 50)
	private int userID;
	
	@Column(name = "firstName", nullable = false, length = 50)
	private String firstName;
	
	@Column(name = "lastName", nullable = false, length = 50)
	private String lastName;
	
	@Column(name = "email", nullable = false, length = 11)
	private String email;
	
	@Column(name = "phone", length = 255)
	private int phone;
	
	@Column(name = "address", length = 100)
	private String address;
	
	@Column(name = "city", length = 6)
	private String city;
	
	@Column(name = "postcode", length = 3)
	private String postcode;
	
	@Column(name = "countrycode", length = 3)
	private String countrycode;
	
	@Column(name = "languages", length = 150)
	private String languages;
	
	@Column(name = "interests", length = 150)
	private String interests;
	
	@Column(name = "intuserID", length = 150)
	private int intuserID;	
	
	public int getResumeID() {
		return resumeID;
	}

	public void setResumeID(int resumeID) {
		this.resumeID = resumeID;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getCountrycode() {
		return countrycode;
	}

	public void setCountrycode(String countrycode) {
		this.countrycode = countrycode;
	}

	public String getLanguages() {
		return languages;
	}

	public void setLanguages(String languages) {
		this.languages = languages;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public int getIntuserID() {
		return intuserID;
	}

	public void setIntuserID(int intuserID) {
		this.intuserID = intuserID;
	}
	
	@ManyToMany(mappedBy = "resumes")
	private List<User> users;

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	
	@OneToMany
	@JoinColumn(name = "experienceID")
	private List<Experience> experiences;

	public List<Experience> getExperiences() {
		return experiences;
	}

	public void setExperiences(List<Experience> experiences) {
		this.experiences = experiences;
	}
	
	@OneToMany
	@JoinColumn(name = "educationID")
	private List<Education> educations;

	public List<Education> getEducations() {
		return educations;
	}

	public void setEducations(List<Education> educations) {
		this.educations = educations;
	}
	
	
	
	
	

}
